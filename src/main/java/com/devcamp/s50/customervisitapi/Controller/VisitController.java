package com.devcamp.s50.customervisitapi.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.customervisitapi.Service.VisitService;
import com.devcamp.s50.customervisitapi.model.Visit;

@RequestMapping("/")
@CrossOrigin
@RestController
public class VisitController {
    @Autowired
    private VisitService visitService ;

    @GetMapping("/visits")
    public ArrayList<Visit> getAllVisitApi(){
        ArrayList<Visit> AllVisit = visitService.getAllVisit();
        return AllVisit ;
    }
}
