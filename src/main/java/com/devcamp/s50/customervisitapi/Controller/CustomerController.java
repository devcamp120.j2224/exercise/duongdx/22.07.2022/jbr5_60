package com.devcamp.s50.customervisitapi.Controller;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.customervisitapi.Service.CustomerService;
import com.devcamp.s50.customervisitapi.model.Customer;



@RestController
@RequestMapping("/")
@CrossOrigin
public class CustomerController {
    @Autowired 
    private CustomerService customerService;
    @GetMapping("/customers")
    public ArrayList<Customer> getCustomer(){
        ArrayList<Customer> ALLcustomer = customerService.getAllCustomer();
        return ALLcustomer ;
    }
}

