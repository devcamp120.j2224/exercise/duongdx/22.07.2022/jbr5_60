package com.devcamp.s50.customervisitapi.model;

import java.util.Date;

public class Visit {
    private Customer customer ;
    private Date date ;
    private double serviceExpensive;
    private double productExpensive;

    public Visit(Date date) {
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpensive() {
        return serviceExpensive;
    }

    public void setServiceExpensive(double serviceExpensive) {
        this.serviceExpensive = serviceExpensive;
    }

    public double getProductExpensive() {
        return productExpensive;
    }

    public void setProductExpensive(double productExpensive) {
        this.productExpensive = productExpensive;
    }

    
    public double getTotal(){
        return serviceExpensive * productExpensive ;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpensive=" + productExpensive
                + ", serviceExpensive=" + serviceExpensive + "]";
    }
    
    
}
