package com.devcamp.s50.customervisitapi.Service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.customervisitapi.model.Visit;

@Service
public class VisitService {
    @Autowired
    private CustomerService customerService ;
    Visit visit1 = new Visit(new Date());
    Visit visit2 = new Visit(new Date());
    Visit visit3 = new Visit(new Date());
    public ArrayList<Visit> getAllVisit(){
        ArrayList<Visit> AllVisit = new ArrayList<>();
        visit1.setCustomer(customerService.khach1);
        visit2.setCustomer(customerService.khach2);
        visit3.setCustomer(customerService.khach3);
        AllVisit.add(visit1);
        AllVisit.add(visit2);
        AllVisit.add(visit3);

        return AllVisit ;
    }
}
