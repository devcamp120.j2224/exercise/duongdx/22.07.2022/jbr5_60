package com.devcamp.s50.customervisitapi.Service;

import java.util.ArrayList;
import org.springframework.stereotype.Service;
import com.devcamp.s50.customervisitapi.model.Customer;

@Service
public class CustomerService {
    Customer khach1 = new Customer("duong");
    Customer khach2 = new Customer("Linh");
    Customer khach3 = new Customer("Dao");

    public ArrayList<Customer> getAllCustomer(){
        ArrayList<Customer> AllCustomer = new ArrayList<>();
        AllCustomer.add(khach1);
        AllCustomer.add(khach2);
        AllCustomer.add(khach3);
        return AllCustomer ;
    }
}
